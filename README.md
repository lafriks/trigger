# Woodpecker CI trigger plugin

<a href="https://codeberg.org/woodpecker-plugins/trigger">
  <img alt="Get it on Codeberg" src="https://codeberg.org/Codeberg/GetItOnCodeberg/media/branch/main/get-it-on-neon-blue.png" height="60">
</a>

Woodpecker CI plugin to trigger repository builds or deployments. For the usage information and a listing of the available options please take a look at [the docs](docs.md) or [woodpecker-ci.org](https://woodpecker-ci.org/plugins/Trigger).

This plugin is a fork of [drone-plugins/drone-downstream](https://github.com/drone-plugins/drone-downstream/).

## Build

Build the binary with the following command:

```console
make build
```

## Docker

Build the Docker image with the following command:

```console
docker buildx build \
  --label org.label-schema.build-date=$(date -u +"%Y-%m-%dT%H:%M:%SZ") \
  --label org.label-schema.vcs-ref=$(git rev-parse --short HEAD) \
  --platform linux/amd64 \
  --file Dockerfile.multiarch --tag woodpeckerci/plugin-trigger .
```

## Maintainers

This plugin is maintained by @6543 and @lafriks.

## Contributors

Special thanks goes to all [contributors](https://codeberg.org/woodpecker-plugins/trigger/activity). If you would like to contribute,
please see the [instructions](https://codeberg.org/woodpecker-plugins/trigger/src/branch/main/CONTRIBUTING.md).

## License

This project is licensed under the Apache-2.0 License - see the [LICENSE](https://codeberg.org/woodpecker-plugins/trigger/src/branch/main/LICENSE) file for details.
