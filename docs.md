---
name: Trigger
icon: https://codeberg.org/woodpecker-plugins/trigger/media/branch/main/logo.png
description: plugin to trigger Woodpecker CI builds or deployments
author: Woodpecker Authors
tags: [woodpecker, trigger, pipeline]
containerImage: woodpeckerci/plugin-trigger
containerImageUrl: https://hub.docker.com/r/woodpeckerci/plugin-trigger
url: https://codeberg.org/woodpecker-plugins/trigger
---

Woodpecker CI plugin to trigger repository builds or deployments. This plugin is a fork of [drone-plugins/drone-downstream](https://github.com/drone-plugins/drone-downstream/).

## Features

- Trigger one or multiple pipelines
- Trigger deploy action
- Pass variables to pipelines

## Settings

| Settings Name             | Default           | Description
| --------------------------| ----------------- | --------------------------------------------
| `server`                  | current server    | sets Woodpecker CI server URL to call
| `token`                   | *none*            | sets Woocpecker CI API token for server
| `repositories`            | *none*            | sets repositories name whose pipelines to trigger
| `deploy`                  | *none*            | sets the environment to deploy to
| `params`                  | *none*            | sets list of params (key=value or file paths of params) to pass to triggered builds
| `params-from-env`         | *none*            | sets list of environment variables to pass to triggered builds
| `wait`                    | `false`           | sets to wait for any currently running builds to finish
| `timeout`                 | `60s`             | sets how long to wait on any currently running builds
| `last-successful`         | `false`           | sets to trigger last successful build

### repositories

Multiple repositories whose pipelines need to be triggered can be provided.
Also it's possible to provide specific branch to trigger can be specified with `@` separator, ex. `owner/repo@branch`.

## Examples

```yaml
  trigger-downstream:
    image: woodpeckerci/plugin-trigger
    settings:
      repositories:
        - octocat/hello-world@master
        - octocat/sample
      token:
        from_secret: woodpecker_token
```

```yaml
  publish:
    image: woodpeckerci/plugin-trigger
    settings:
      repositories:
        - octocat/hello-world
      deploy: production
      params:
        - DATABASE=prod
      wait: true
      token:
        from_secret: woodpecker_token
    when:
      branch: ${CI_REPO_DEFAULT_BRANCH}
      event: push
```
