# Changelog

## [0.2.3](https://codeberg.org/woodpecker-plugins/trigger/releases/tag/v0.2.3) - 2024-09-29

### ❤️ Thanks to all contributors! ❤️

@6543, @lafriks, @qwerty287, @woodpecker-bot

### 📚 Documentation

- Add CODEOWNERS [[#20](https://codeberg.org/woodpecker-plugins/trigger/pulls/20)]
- Fix author key [[#18](https://codeberg.org/woodpecker-plugins/trigger/pulls/18)]
- Add logo [[#17](https://codeberg.org/woodpecker-plugins/trigger/pulls/17)]

### 📦️ Dependency

- chore(deps): update woodpeckerci/plugin-docker-buildx docker tag to v4 [[#21](https://codeberg.org/woodpecker-plugins/trigger/pulls/21)]
- chore(deps): update woodpeckerci/plugin-docker-buildx docker tag to v3 [[#19](https://codeberg.org/woodpecker-plugins/trigger/pulls/19)]

### Misc

- Use ready-release-go plugin [[#24](https://codeberg.org/woodpecker-plugins/trigger/pulls/24)]
- Migrate to github.com/urfave/cli/v3 [[#23](https://codeberg.org/woodpecker-plugins/trigger/pulls/23)]
