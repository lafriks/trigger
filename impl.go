// Copyright 2020, the Drone Plugins project authors.
// Copyright 2023 Woodpecker Authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package trigger

import (
	"context"
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/joho/godotenv"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"go.woodpecker-ci.org/woodpecker/v3/woodpecker-go/woodpecker"
	"golang.org/x/oauth2"
)

var errBuildNotFound = fmt.Errorf("build not found")

// Validate handles the settings validation of the plugin settings.
func (p *Plugin) Validate() error {
	if len(p.Settings.Token) == 0 {
		return fmt.Errorf("you must provide access token")
	}

	p.Settings.server = getServerWithDefaults(p.Settings.Server, p.Metadata.System.Link)
	if len(p.Settings.server) == 0 {
		return fmt.Errorf("you must provide Woodpecker CI server URL")
	}

	if p.Settings.Wait && p.Settings.LastSuccessful {
		return fmt.Errorf("only one of wait and last_successful can be true; choose one")
	}

	var err error
	p.Settings.params, err = parseParams(p.Settings.Params)
	if err != nil {
		return fmt.Errorf("unable to parse params: %s", err)
	}

	for _, k := range p.Settings.ParamsEnv {
		v, exists := os.LookupEnv(k)
		if !exists {
			return fmt.Errorf("param_from_env %s is not set", k)
		}

		p.Settings.params[k] = v
	}

	return nil
}

func findFirstBuild(client woodpecker.Client, slug string, cond func(*woodpecker.Pipeline) bool) (*woodpecker.Pipeline, error) {
	repo, err := client.RepoLookup(slug)
	if err != nil {
		return nil, fmt.Errorf("unable to get repository: %w", err)
	}
	// TODO: use pagination
	builds, err := client.PipelineList(repo.ID, woodpecker.PipelineListOptions{})
	if err != nil {
		return nil, fmt.Errorf("unable to get build list: %w", err)
	}

	for _, b := range builds {
		if cond(b) {
			return b, nil
		}
	}

	return nil, errBuildNotFound
}

// Execute provides the implementation of the plugin.
func (p *Plugin) Execute(ctx context.Context) error {
	if err := p.Validate(); err != nil {
		return err
	}

	config := new(oauth2.Config)

	auther := config.Client(
		context.WithValue(ctx, oauth2.HTTPClient, p.HTTPClient()),
		&oauth2.Token{
			AccessToken: p.Settings.Token,
		},
	)

	client := woodpecker.NewClient(p.Settings.server, auther)

	for _, entry := range p.Settings.Repos {

		// parses the repository name in slug@branch format
		slug, branch := parseRepoBranch(entry)
		if len(slug) == 0 {
			return fmt.Errorf("unable to parse repository name %s", entry)
		}

		// check for mandatory build no during deploy trigger
		if len(p.Settings.Deploy) != 0 {
			if branch == "" {
				return fmt.Errorf("build no or branch must be mentioned for deploy, format repository@build/branch")
			}
			if _, err := strconv.Atoi(branch); err != nil && !p.Settings.LastSuccessful {
				return fmt.Errorf("for deploy build no must be numeric only " +
					" or for branch deploy last_successful should be true," +
					" format repository@build/branch")
			}
		}

		waiting := false

		timeout := time.After(p.Settings.Timeout)

		ticker := time.NewTicker(1 * time.Second)
		defer ticker.Stop()

		var err error

		// Keep trying until we're timed out, successful or got an error
		// Tagged with "I" due to break nested in select
	I:
		for {
			select {
			// Got a timeout! fail with a timeout error
			case <-timeout:
				return fmt.Errorf("timed out waiting on a build for %s", entry)
			// Got a tick, we should check on the build status
			case <-ticker.C:
				// first handle the deploy trigger
				if len(p.Settings.Deploy) != 0 {
					var build *woodpecker.Pipeline
					if p.Settings.LastSuccessful {
						// Get the last successful build of branch
						build, err = findFirstBuild(client, slug, func(b *woodpecker.Pipeline) bool {
							return b.Branch == branch && b.Status == woodpecker.StatusSuccess && b.Event == woodpecker.EventPush
						})
						if err != nil {
							return fmt.Errorf("unable to get last successful build for %s: %w", entry, err)
						}
					} else {
						repo, err := client.RepoLookup(slug)
						if err != nil {
							return fmt.Errorf("unable to get repository: %w", err)
						}
						// Get build by number
						buildNumber, _ := strconv.ParseInt(branch, 10, 64)
						build, err = client.Pipeline(repo.ID, buildNumber)
						if err != nil {
							return fmt.Errorf("unable to get requested build %v for deploy for %s", buildNumber, entry)
						}
					}
					if p.Settings.Wait && !waiting && (build.Status == woodpecker.StatusRunning || build.Status == woodpecker.StatusPending) {
						log.Info().Msgf("BuildLast for repository: %s, returned build number: %v with a status of %s. Will retry for %v.", entry, build.Number, build.Status, p.Settings.Timeout)
						waiting = true
						continue
					}
					if (build.Status != woodpecker.StatusRunning && build.Status != woodpecker.StatusPending) || !p.Settings.Wait {
						repo, err := client.RepoLookup(slug)
						if err != nil {
							return fmt.Errorf("unable to get repository: %w", err)
						}
						// start a new deploy
						_, err = client.Deploy(repo.ID, build.Number, woodpecker.DeployOptions{DeployTo: p.Settings.Deploy, Params: p.Settings.params})
						if err != nil {
							if waiting {
								continue
							}
							return fmt.Errorf("unable to trigger deploy for %s - err %v", entry, err)
						}
						log.Info().Dict("params", logParams(p.Settings.params, p.Settings.ParamsEnv)).
							Msgf("Starting deploy for %s env - %s build - %d", slug, p.Settings.Deploy, build.Number)

						break I
					}
				}

				// get the latest build for the specified repository
				repo, err := client.RepoLookup(slug)
				if err != nil {
					return fmt.Errorf("unable to get repository: %w", err)
				}
				build, err := client.PipelineLast(repo.ID, woodpecker.PipelineLastOptions{Branch: branch})
				if err != nil {
					if waiting {
						continue
					}
					return fmt.Errorf("unable to get latest build for %s: %s", entry, err)
				}
				if p.Settings.Wait && !waiting && (build.Status == woodpecker.StatusRunning || build.Status == woodpecker.StatusPending) {
					log.Info().Msgf("BuildLast for repository: %s, returned build number: %v with a status of %s. Will retry for %v.", entry, build.Number, build.Status, p.Settings.Timeout)
					waiting = true
					continue
				} else if p.Settings.LastSuccessful && build.Status != woodpecker.StatusSuccess {
					build, err = findFirstBuild(client, slug, func(b *woodpecker.Pipeline) bool {
						return b.Branch == branch && b.Status == woodpecker.StatusSuccess && b.Event == woodpecker.EventPush
					})
					if err != nil {
						return fmt.Errorf("unable to get last successful build for %s: %w", entry, err)
					}
				}

				if (build.Status != woodpecker.StatusRunning && build.Status != woodpecker.StatusPending) || !p.Settings.Wait {
					repo, err := client.RepoLookup(slug)
					if err != nil {
						return fmt.Errorf("unable to get repository: %w", err)
					}
					// rebuild the latest build
					_, err = client.PipelineStart(repo.ID, build.Number, woodpecker.PipelineStartOptions{Params: p.Settings.params})
					if err != nil {
						if waiting {
							continue
						}
						return fmt.Errorf("unable to trigger build for %s: %w", entry, err)
					}
					log.Info().Dict("params", logParams(p.Settings.params, p.Settings.ParamsEnv)).
						Msgf("Restarting build %d for %s", build.Number, entry)

					break I
				}
			}
		}
	}

	return nil
}

func parseRepoBranch(repo string) (string, string) {
	slug, branch, _ := strings.Cut(repo, "@")
	return slug, branch
}

func parseParams(paramList []string) (map[string]string, error) {
	params := make(map[string]string)
	for _, p := range paramList {
		name, value, ok := strings.Cut(p, "=")
		if ok {
			params[name] = value
		} else if _, err := os.Stat(name); os.IsNotExist(err) {
			return nil, fmt.Errorf(
				"invalid param '%s'; must be KEY=VALUE or file path",
				name,
			)
		} else {
			fileParams, err := godotenv.Read(name)
			if err != nil {
				return nil, err
			}

			for k, v := range fileParams {
				params[k] = v
			}
		}
	}

	return params, nil
}

func logParams(params map[string]string, paramsEnv []string) *zerolog.Event {
	dict := zerolog.Dict()
	if len(params) == 0 {
		return dict
	}
	for k, v := range params {
		fromEnv := false
		for _, e := range paramsEnv {
			if k == e {
				fromEnv = true
				break
			}
		}
		if fromEnv {
			v = "[from-environment]"
		}
		dict.Str(k, v)
	}
	return dict
}

func getServerWithDefaults(server, link string) string {
	if len(server) != 0 {
		return server
	}

	return link
}
