module codeberg.org/woodpecker-plugins/trigger

go 1.23.4

toolchain go1.23.5

require (
	codeberg.org/woodpecker-plugins/go-plugin v0.7.0
	github.com/joho/godotenv v1.5.1
	github.com/rs/zerolog v1.33.0
	github.com/urfave/cli/v3 v3.0.0-beta1
	go.woodpecker-ci.org/woodpecker/v3 v3.0.1
	golang.org/x/oauth2 v0.25.0
)

require (
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	golang.org/x/net v0.34.0 // indirect
	golang.org/x/sys v0.29.0 // indirect
)
